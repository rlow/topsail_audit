module Audit

  class CurrentUserForModelsFilter

    def around(controller)
      ActiveRecord::Base.current_user = controller.current_user_profile
      ActiveRecord::Base.current_request_uuid = controller.request.uuid
      yield
      ActiveRecord::Base.current_user = nil
       ActiveRecord::Base.current_request_uuid = nil
    end

  end

end