# This file adds audit log support to ActiveRecords by way of the "acts_as_auditable" class method.

class ActiveRecord::Base

  # Add a "before_create" callback to automatically set 'created_by_id' to the ID of the
  # current user if the ActiveRecord has an "created_by_id" attribute.
  before_create :set_created_by_id

  # Add a "before_save" callback to automatically set 'updated_by_id' to the ID of the
  # current user if the ActiveRecord has an "updated_by_id" attribute.
  before_save :set_updated_by_id

  # Wires up an ActiveRecord to create AuditLog records whenever its attributes are changed, as well as
  # whenever other records are added to or removed from any of its associations.
  #
  # IMPORTANT NOTE
  #
  # This methods dynamically registers "after_add" and "after_remove" callbacks with all of the
  # ActiveRecord's HABTM associations.  For this to work, "acts_as_auditable" must be called AFTER all
  # the associations have been declared.  It cannot add these callbacks to associations that have
  # not yet been defined.
  #
  def self.acts_as_auditable

    # Don't set up the attribute auditing more than once, or you'll get duplicate audit log rows

    unless auditable?
      before_save :audit_log_before_save
      after_create :audit_log_after_create
      after_update :audit_log_after_update
      after_destroy :audit_log_after_destroy
    end

    # To avoid setting up auditing for the same association more than once, we must
    # keep track of which ones we've done in a variable that will be accessible
    # by subclasses

    unless auditable?
      class_attribute(:audited_associations)
      self.audited_associations = []
    end

    reflect_on_all_associations.each do | assoc |

      association_name = assoc.name.to_s

      if (assoc.macro == :has_and_belongs_to_many) && !audited_associations.include?(association_name)
        add_association_callback(association_name, 'add')
        add_association_callback(association_name, 'remove')
        audited_associations << association_name
      end

    end

  end

  # Whether or not this class has been set up to be auditable
  def self.auditable?
    respond_to?(:audited_associations)
  end

  private

  # Helper method for adding association callbacks.
  #
  # WARNING This method depends too much on the inner workings of the Rails association code.
  #
  def self.add_association_callback(association_name, stage)

    callback_name = "after_#{stage}_for_#{association_name}".to_sym

    unless respond_to?(callback_name)
      class_attribute(callback_name)
      self.send("#{callback_name}=", [])
    end

    callbacks = self.send(callback_name)
    callbacks << Proc.new do | record, associated |
      record.send("audit_log_after_association_#{stage}", associated, association_name)
    end

    self.send("#{callback_name}=", callbacks)

  end

  # Audit releated "before_save" callback.  This saves the "changes" hash as a member variable.
  # This is needed because the save itself will clear out the changes and we need to remember
  # what they were for audit_log_after_create and audit_log_after_update
  def audit_log_before_save
    @changes_for_audit = real_changes
  end

  # "after_create" callback that creates a new AttributesAuditLog
  def audit_log_after_create
    add_attributes_audit_log(AuditLog::CREATE_ACTION)
  end

  # "after_update" callback that creates a new AttributesAuditLog
  def audit_log_after_update
    add_attributes_audit_log(AuditLog::UPDATE_ACTION)
  end

  # "after_destroy" callback that creates a new AttributesAuditLog to keep track of the deleted record
  def audit_log_after_destroy
    add_attributes_audit_log(AuditLog::DELETE_ACTION)
  end

  # "after_add" association callback the creates a new AssociationAuditLog for the added id
  def audit_log_after_association_add(associated, association_name)
    add_association_audit_log(AuditLog::CREATE_ACTION, associated, association_name)
  end

  # "after_remove" association callback the creates a new AssociationAuditLog for the removed id
  def audit_log_after_association_remove(associated, association_name)
    add_association_audit_log(AuditLog::DELETE_ACTION, associated, association_name)
  end

  # Helper method to create a AttributesAuditLog
  def add_attributes_audit_log(action)

    unless action == AuditLog::UPDATE_ACTION and (@changes_for_audit.nil? or @changes_for_audit.empty?)
      AttributesAuditLog.create!( audited: self,
                                  action: action,
                                  attributes_json: self.to_json,
                                  changes_json: @changes_for_audit && @changes_for_audit.to_json,
                                  request_uuid: ActiveRecord::Base.current_request_uuid )
      @changes_for_audit = nil # don't need this any more
    end

  end

  # Helper method to create an AssociationAuditLog
  def add_association_audit_log(action, associated, association_name)

    unless new_record? or associated.new_record?
      AssociationAuditLog.create!( audited: self,
                                   action: action,
                                   association_name: association_name,
                                   associated: associated,
                                   request_uuid: ActiveRecord::Base.current_request_uuid )
    end

  end

  # Sometimes the "changes" Hash with have fields that didn't really change, because
  # it gets confused by empty strings being nilified.  So as a workaround we filter
  # out changes that aren't really changes
  def real_changes
    real = changes
    real = {} if real.nil?
    real.delete_if { |key, value| value[0].nil? and value[1].blank? }
  end

  def set_created_by_id
    if self.respond_to?('created_by_id') and !current_user.nil?
      self.created_by_id = current_user.id
    end
  end

  def set_updated_by_id
    if self.respond_to?('updated_by_id') and !current_user.nil? and (self.new_record? || !real_changes.empty?)
      self.updated_by_id = current_user.id
    end
  end

end
