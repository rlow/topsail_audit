class AuditLog < ActiveRecord::Base
  
  CREATE_ACTION = 'create'
  UPDATE_ACTION = 'update'
  DELETE_ACTION = 'delete'
  
  belongs_to :audited, :polymorphic => true
  belongs_to :created_by, :class_name => 'UserProfile', :foreign_key => 'created_by_id'
   
  # strips_and_converts_blank_to_nil :audited_type, :action
  
  validates :audited_type, :presence => true, :length => { :maximum => 100 }
  validates :audited_id,   :presence => true, :numericality => { :only_integer => true }
  validates :action,       :presence => true, 
                           :length => { :maximum => 10 }, 
                           :inclusion => { :in => [ CREATE_ACTION, UPDATE_ACTION, DELETE_ACTION ]}
  
  # backward compatibility
  def record_class
    audited_type.constantize
  end
  
end