class AttributesAuditLog < AuditLog

  belongs_to :associated, :polymorphic => true

  def record_changes
    changes_json
  end

end