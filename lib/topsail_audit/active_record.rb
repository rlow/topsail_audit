class ActiveRecord::Base

  def current_user
    Thread.current[:current_user]
  end

  def self.current_user
    Thread.current[:current_user]
  end

  def self.current_user= (who)
    Thread.current[:current_user] = who
  end

  def self.current_request_uuid
    Thread.current[:current_request_uuid]
  end

  def self.current_request_uuid= (what)
    Thread.current[:current_request_uuid] = what
  end

end
