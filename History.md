1.4.0
=====
  * change from YAML and XML to native json data type in database (Postgres only?)
  * store request_uuid in audit log records

1.3.1
=====
  * remove depenency on topsail_support

1.3.0
=====

  * remove :attr_accessible and get rid of this terrible attribute protection solution; you better use strong_parameters in your app!

1.2.0 (breaks backwords compatibility with existing audit_log tables!)
=====

  * make acts_as_auditable compatible with Rails >= 3.2
  * take advantage of polymorphic associations. Audited records are now available as audit_log.audited,
    audit log records can be made available to any record acting as_auditable via
          has_many :audit_logs, :as => :audited
  * only audit HABTM associations for audited classes. If you want to audit other associations, you should
    audit the associated class!
  
1.1.0
=====

  * don't rely on Topsail::Authentication anymore, now we can use Devise
    - add Audit::CurrentUserForModelsFilter around filter to make devise current_user available to models
  
1.0.1
=====

  * always set the updated_by_id attribute for new records, if possible
  
1.0.0
=====

  * stable version, running in production systems for years
