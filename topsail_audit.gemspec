# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "topsail_audit/version"

Gem::Specification.new do |s|
  s.name        = "topsail_audit"
  s.version     = Topsail::Audit::VERSION
  s.authors     = ["Mark Roghelia", "Jan Schroeder"]
  s.email       = ["mroghelia@topsailtech.com", "jschroeder@topsailtech.com"]
  s.summary     = %q{ActiveRecord audit log support}
  s.files         = `git ls-files`.split("\n")
  s.require_paths = ["lib"]
  # s.add_dependency('topsail_support', '>= 1.0.0')
end
