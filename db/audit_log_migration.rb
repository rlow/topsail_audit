class AuditLogMigration < ActiveRecord::Migration

  def self.up

    create_table :audit_log do |t|
      t.string     :type, :limit => 50, :null => false
      t.string     :request_uuid,
      t.references :audited, :polymorphic => true, :null => false
      t.string     :action, :null => false, :limit => 10
      t.json       :attributes_json
      t.json       :changes_json
      t.string     :association_name, :limit => 200
      t.references :associated, :polymorphic => true

      t.references :created_by, :class_name => 'UserProfile', :foreign_key => true
      t.timestamp  :created_at, :null => false
    end

    add_index :audit_log, [ :audited_type, :audited_id ], :name => 'audit_log_idx'
    add_index :audit_log, [ :created_by_id, :created_at ], :order => {:created_by_id => :asc, :created_at => :desc}, :name => 'audit_log_who_when_idx'
    add_index :audit_log, [ :created_at ], :order => {:created_at => :desc}, :name => 'audit_log_when_idx'
    add_index :audit_log, [:request_uuid, :created_at], order: {created_at: :desc}

    execute "alter table audit_log add constraint audit_log_user_profile_fk foreign key (created_by_id) references user_profile(id)"
    execute "alter table audit_log add constraint audit_log_ck1 check (action in ('create', 'update', 'delete'))"
    execute "alter table audit_log add constraint audit_log_ck2 check (type in ('AttributesAuditLog', 'AssociationAuditLog'))"

  end

  def self.down
    drop_table :audit_log
  end

end